---
document set: DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Directions and Legend
author: Jonah Silas Sheridan, Lisa Jervis
last modified: 11/27/17
version: "2.01, PEER REVIEWED"
---
# Directions for Use

## Before using these checklists

The first item in the set is a [Digital Security Readiness Assessment Tool](2_readiness_assessment_tool.md). We recommend you use this tool to see how prepared your organization is to take on digital security upgrades.

Since many foundational technology management and operations tasks underlie digital security capacity, or are even digital security tasks themselves, this tool is broken out into three categories: cultural hallmarks of security success, information technology operations that support security outcomes, and digital security baseline capacities. Follow the directions for the tool itself to find out how prepared your organization is before proceeding.

If your level of preparation is low in any broad category, individual item, or overall, that is the place to begin building your digital security capacity. If you decide to move on to the checklists in the meantime, be aware that your outcomes may be limited by these organizational dynamics.

Consistent attention to and maintence of these foundational capacities is necessary for success in digital security efforts, so be sure to have a plan to keep improving your organization in these areas.

## How to use the checklists

Once your organization has the readiness needed to take on some new security practices, you should proceed to the checklists themselves. They are compiled around specific topical areas, namely device security, authentication, wireless networks, email, and, for those who use it, G Suite.

We recommend pursuing these checklists in the order they are presented in this set, except that if you are using a specific platform, tool, or technology with dedicated a checklist of its own (such as G Suite) in which case you should start there.

All items on the these checklists are meant to be actionable and accessible; each checklist item includes a brief explanation of what it means as well as, where possible, next steps for implementation.

The icons accompanying each item will help you identify how hard to manage (as indicated by the :rocket: icons), technically difficult (as indicated by the :wrench: icons) and disruptive (as indicated by the :fire: icons) a given step might be to undertake. Be sure to pick practices that match the time and resources your organization has available.

Be especially aware of the :fire: rating, as it indicates disruption--only take on the practices with multiple :fire: icons if you have the space to spend time as an organization absorbing training overhead and work flow transformation.

## Legend

## :heavy_check_mark:  
This check mark icon flags places for you to record actions you have taken. Cross them off or circle them as you go.

## :rocket:  
This rocket icon represents the amount of technology management overhead required to implementation the item, in terms of the attention of leadership and technology-responsible staff inside an organization. One-rocket items should be doable by most technology capable organizations that have achieved other basic technology competency. Items with two rockets may require additional time carved out beyond what regular operations demand, as they possibly require some outside assistance and work flow shifts. Three rockets will require significant organizational commitment of resources to manage the project of implementing the recommendation, for support of renewed work flows, and to interface with technical assistance. Items with four rockets are only for organizations ready to take on advanced security practices, including a part-to-full-time dedicated project manager as well as the ongoing commitment of human and other resources needed for process management, technical configuration, training, and ongoing support.

## :wrench:    
This wrench icon represents the amount of technical skill needed to undertake the practice. One wrench means most skilled computer users can do, or be trained to do, the task. Two wrenches require “power user” technical skills, often found in the “Accidental Techie” on staff. Three wrenches will require a person experienced in technical support or systems administration to do the work. Four wrenches means you will need a technical support person or internal staffer with significant skills in networking or security to undertake the practice.

## :fire:  
This fire icon represents the amount of work flow disruption taking on this task entails, and consequently how much staff time for documentation, training, and practice to achieve work flow shifts is required. One-flame items will be mostly innocuous and staff can be trained in a brief session. Two flames means the practice will require more training and can disrupt existing work flows. Three flames signals that significant work flow shifts and training will be required to undertake the practice. Four flames means the task will disrupt work flow completely and is only for organizations where security is of far greater importance than efficiency or convenience.

***Although these practices are highly recommended, they do not in and of themselves constitute a successful security practice. Information security is an ongoing process of managing risk and no list of procedures is an adequate replacement for a thorough review of what information you are protecting, why, and from whom, paired with an organizational commitment to shifting operations to mitigate risk. Information Ecology, RoadMap Consulting, and Common Counsel are not liable for negative outcomes associated with following these practices.***

---
document set: DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Wireless Network Safety Checklist
author: Jonah Silas Sheridan, Lisa Jervis
last modified: 11/27/17
version: "2.01, PEER REVIEWED"
---

# Wireless Network Safety Checklist

## Introduction

*This checklist comes from the Weathering the Storms toolkit, which contains wraparound documentation including an [introduction](1_checklist_introduction.md), [frequently asked questions](C_FAQ.md), and a [glossary](A_glossary.md) where you can look up any terms that are unfamiliar to you. This is a community-driven document set with the latest version always at [ecl.gy/sec-check](https://ecl.gy/sec-check). We welcome your feedback via RoadMap, or our contact form at [iecology.org/contact/](https://iecology.org/contact/).*

This checklist provides a number of practices that can help protect you and your staff when using wireless networks such as those in offices and co-work spaces as well as public places such as hotels, cafés, and airports. Because there are so many ways that wireless networks can be compromised, you should treat all wireless networks as having limited security. You are always safest directly wired into networks that you own and/or control.

**If performing work using sensitive or confidential information, including anything that is required to be protected by law (such as personal health information, employment records, and credit card numbers), you are best off avoiding the use of wireless networks for those tasks if possible and should never use a free or public wireless network for that work, unless you are using a VPN. (VPNs are covered below.)**

## Key

:heavy_check_mark: Record actions  
:rocket: Implementation management overhead   
:wrench: Technical skill level required  
:fire: Work flow disruption for staff

## Wireless Network Safety Tasks

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Prefer Firefox or Chrome browsers. Only use Internet Explorer and Safari when required. Keep all web browser software, including extensions, updated to the latest version.**  
:rocket: :wrench: :fire:  
*Internet Explorer has had a much higher incidence of vulnerabilities than Chrome and Firefox, while Safari has suffered some recent security concerns. Although nearly all of the latest browsers support “certificate pinning,” which makes it harder to intercept secure connections, [Chrome](https://google.com/chrome) and [Firefox](https://getfirefox.com/) have led the development of this important feature.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Install the HTTPS Everywhere extension on all of the web browsers you use.**  
:rocket: :wrench: :fire:  
*The "s" in HTTPS stands for "secure," and when you see "https://" rather than "http://" in your browser's address bar, it means that you are securely connected to the site you are visiting: The information being sent back and forth between your browser and the site's server is encrypted and so cannot be seen by others on the network or the operator of the network itself. The browser extension HTTPS Everywhere, produced by the [Electronic Frontier Foundation](https://eff.org) (https://eff.org) forces your browser to connect using HTTPS instead of HTTP to any site that makes an HTTPS connection available, thus increasing the proportion of your traffic that cannot be viewed or altered by others on your network. You can install that plugin at [https://www.eff.org/HTTPS-EVERYWHERE](https://www.eff.org/HTTPS-EVERYWHERE).*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Install Privacy Badger, a browser add-on that will limit the “cookies”--small persistent chunks of information--set on your computer by websites.**  
:rocket: :wrench: :fire: :fire:  
*Privacy Badger (also produced by the [Electronic Frontier Foundation](https://eff.org)) is designed to help reduce the privacy breaches and tracking that come with the use of cookies. These cookies can be transferred insecurely and so can, if poorly implemented, expose login credentials or other information in transit. As an extra benefit, using this extension will increase your privacy and reduce the extent to which you are tracked online. Download it at [privacybadger.org](https://privacybadger.org).*

*Note that if you are using integrations between different web-based systems in your work (for example, connecting file-sharing systems such as Google or Box to project management systems such as Asana or Basecamp), you will need to tune your Privacy Badger settings for those sites to keep the integrations working properly.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**When you have a choice, pick wireless networks that use a password, ideally a unique one for each person connecting, and those that use WPA or WPA2 encryption rather than WEP encryption.**  
:rocket: :wrench: :fire:  
*A password on a wireless network means the information moving across it is less easily captured and decoded by someone nearby. However, in most cases everyone with that password can at least see some parts of your network connections--but if everyone has a unique password this becomes quite hard to do. WPA and WPA2 offer stronger protection than WEP, which is now relatively easily compromised. Most computers offer an easy way to view what encryption is in use on a given network. In OSX, hold down the Option key and click the wireless indicator in the top right corner to reveal extra information about each wireless network. The method for viewing these details is different in each version of Windows, so ask your tech support provider for assistance for the software you use.
Note that some broad attacks on WPA encryption schemes have recently come to light. Consequently this recommendation has only limited utility, and for sensitive operations a VPN or other encrypted connection is necessary to ensure the confidentiality of your information.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Confirm the network details before you connect.**  
:rocket: :rocket: :wrench: :fire:  
*An attacker can set up an access point with a name similar or identical to a legitimate one, so that you connect to the attacker's network instead of the one you intend. Make sure to ask the proprietor of a public network what the network name and password are, and connect to the network with that name that accepts that password. This doesn't completely guarantee that the network you are connecting to isn't hostile or compromised, but it makes the difficulty of hijacking your connection much higher.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Ensure that the wireless network is not presenting false certificates, and do not import any certificates you are asked to install.**  
:rocket: :rocket: :rocket: :rocket: :wrench: :wrench: :wrench: :fire: :fire: :fire:  
*Increasingly, networks are set up to monitor traffic for various reasons such as ad placement or content filtering. However, this potentially compromises all secure connections, as it allows traffic to be monitored via the same mechanism in what is called a man-in-the-middle (MITM) attack. Under these circumstances the network device will ask you to install a certificate that it controls and then will replace the security certificate from the service you are connecting to with the one you installed. Anyone with access to that device can now see any communication between you and that service. Learning to view certificates in your web browser, or installing and learning to use a tool such as [Certificate Patrol](http://patrol.psyced.org/) (http://patrol.psyced.org/), available only for Firefox, will help you identify certificate changes but in normal operation also causes many alert windows to appear as vendors change their certificates.*  

*Google has created documentation for [viewing certificate information in Chrome](https://support.google.com/chrome/answer/95617?hl=en) (https://support.google.com/chrome/answer/95617?hl=en). Mozilla has [similar documentation for Firefox](https://support.mozilla.org/en-US/kb/secure-website-certificate) (https://support.mozilla.org/en-US/kb/secure-website-certificate) as well as some [overall instructions on connection security](https://support.mozilla.org/en-US/kb/how-do-i-tell-if-my-connection-is-secure) (https://support.mozilla.org/en-US/kb/how-do-i-tell-if-my-connection-is-secure).*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**Use a Virtual Private Network (VPN) to securely tunnel out of wireless networks.**  
:rocket: :rocket: :rocket: :wrench: :wrench: :wrench: :fire: :fire: :fire:  
*A VPN creates a secure connection for your computers and mobile devices to use to access the Internet (or an office network). This connection, or tunnel, can be used to hide all information moving between your computers and the Internet (or office network) from the operator or other users of the wireless network. Use of a VPN severely limits your exposure to the owner and operator of the network you are on and so significantly reduces the amount of trust you have to place in them. These factors make VPNs a very effective way to protect your traffic from observation or interception on untrusted networks.*

*A VPN is implemented via a device you own located in your office or at an offsite facility, or that a third party hosts for you. If hosting your own VPN hardware, make sure you budget for ongoing maintenance, licensing, and software updates; otherwise, the device mediating your connection will become a vulnerability instead of a security improvement. Also recognize that in setting up a device to use for VPN connections inside your office, many offsite staff will be dependent on your office Internet line for their work. If this Internet connection is unstable, undersized, or asymmetric (made for downloading more than uploading, such as DSL or residential cable connections), the VPN will not work well for staff. For this reason, paying to locate your VPN device in a data center is the best way of getting a high trust, high-performance VPN in place.*

*Because of the high cost of self-hosted VPNs, most organizations choose to use a third-party VPN service provider to meet this need. This makes budgetary and operational sense; however, it is very important to vet a VPN provider carefully by thoroughly reviewing their policies, understanding their track record in the field, and checking client references. Recognize that unless you set up, run, and maintain your own VPN infrastructure, you are just offloading the trust you don't want to place in the operators of networks you are using to a different third party--the owner and operator of the VPN service. While specific recommendations for VPN providers are outside of the scope of this document, in general, free VPN services, including those available in some app stores, should be avoided. (The adage "If you are not paying for it, you're not the customer--you're the product" holds true here.)*

*Choosing a provider of a VPN and setting up devices to use it are not simple tasks, and they are critically important--a misstep in setup or use can bring your work to a crawl or expose your information. All VPNs add a layer of network traffic and will slow down your Internet access, so your distance to and the bandwidth available from your VPN provider (or your office or data center if hosting your own) will make a difference to performance--and in turn whether people actually use it. The Electronic Frontier Foundation's Surveillance Self-Defense project contains [further information on choosing a VPN](https://ssd.eff.org/en/module/choosing-vpn-thats-right-you) (https://ssd.eff.org/en/module/choosing-vpn-thats-right-you).*

*Consider whether you can absorb the costs to make the speed and trust tradeoffs acceptable to you before choosing to implement a VPN. If you can, the investment in hardware, implementation, setup, and hassle is repaid by a solution that mitigates a range of threats associated with use of untrustworthy networks across many situations.*

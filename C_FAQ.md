---
document set: DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Appendix C: Frequently Asked Questions
author: Jonah Silas Sheridan, Lisa Jervis
last modified: 10/27/2017
version: "2.0, PEER REVIEWED"
---
# Appendix C: Frequently Asked Questions

## Where did this document set come from?

This set of documents was made to help small non-profit organizations improve their digital security outcomes despite limited resources and technical skill availability. The content was commissioned as part of the [Weathering The Storms](http://www.roadmapconsulting.org/WTS) initiative of [RoadMap Consulting](http://www.roadmapconsulting.org), fiscally sponsored by [Common Counsel Foundation](http://commoncounsel.org) of [Oakland, California](https://localwiki.org/oakland/). The content was researched and prepared by Jonah Silas Sheridan and Lisa Jervis, Principals of [Information Ecology](https://iecology.org), a capacity building consultancy specializing in non-profit and movement-building technology management, and was peer-reviewed by generous members of our community. Many other eyes and hands have helped tune the recommendations to ensure technical accuracy and ease of use. We are grateful to all the members of our community that have helped bring these documents to life.

## When was this document set created and last updated?

**This document was last updated in October 2017.**

These documents were originally researched and peer reviewed in fall 2015. Some small edits and a minor 1.1 revision in spring 2017 updated and improved the [Readiness Assessment Tool](2_readiness_assessment_tool.md) and other checklist language based on field experience. A major version 2.0 release was completed in September 2017. This version includes a review, update, and extension of the checklist set. It adds a [Device Security Checklist](4_device_security_checklist.md) and [G Suite Security Checklist](8_gsuite_security_checklist.md), as well as an [Assumed Threat Model](A_threat_model.md) for technical readers. All new content was peer reviewed. Contact [RoadMap Consulting](https://roadmapconsulting.org) or [Information Ecology](https://iecology.org/contact) with questions about this process or content.

**If you have feedback or questions about this document set, its contents, or how to use it, please contact Information Ecology using [our secure contact form](https://iecology.org/contact) or PGP encrypted email to info@iecology.org using [this key](https://iecology.org/0x3C2BACE5E10F3C7A_pub.txt).**

## Why digital security checklists?

While computers have revolutionized and opened all sorts of new possibilities in how non-profits operate, the last several years have begun to reveal to the general public the many risks associated with digital communication and information storage. While all organizations want to protect their information--and that of their partners and allies--few have a strong understanding of the relevant risks and most effective ways to manage them.

**These checklists represent recommendations for a set of baseline digital security practices to help organizations move forward.**

They have been created as a harm-reduction and capacity-building step to meet the common patterns in technical operations in small organizations. We have incorporated information from incident reports, emerging standards, current research, field experience, and community feedback about the work habits of and threats faced by non-profit organizations. The aim is to help organizations improve digital security outcomes by minimizing the easiest-to-exploit vulnerabilities in their systems. This strategy provides protection against many of the common attacks organizations are prey to while also helping create a stronger front against more advanced adversaries with time and resources to invest.

By minimizing the costs and disruption of routine security incidents (such as viruses, malware, ransomware, and phishing) and exposing staff to digital security topics and practices, it is hoped that going through these checklists helps create space for deeper risk analysis and organizational security efforts. By stepping through these checklists, organizations can build their security practice muscles by implementing new, accessible habits and practices. Building this foundational capacity to tune operations is critical to taking on more advanced or disruptive security measures as the threat landscape changes.


## What can't these checklists do for me?

The public-health concept of harm reduction is a useful approach to any situation for which a perfect solution is not available. Despite being an incomplete solution, regular hand washing is an important part of limiting the risk of getting certain illnesses. Similarly, a set of standard best practices represented by checklists cannot mitigate all risks, yet they can help protect you and your organization from some of the serious threats that come with using computers to manage your information. These checklists are meant as a starting point in understanding and responding to the most basic threats computer users face today. They are a useful first step to secure ourselves, our organizations, and our movements, but they are not sufficient. For those of us working in extremely hostile environments; aligned against highly repressive regimes; in closing political spaces; or in high-risk conflicts, disasters, or other unrest, a more rigorous approach is necessary. When significant risks of bodily harm, long-term detention, and death exist, these checklists cannot substitute for a more aggressive and thorough security analysis and response.

Effective security is an ongoing process. It requires consistent practices to be undertaken by all staff, periodic review and adjustment to practices, and strong leadership from board and senior staff. Every organization faces a specific set of threats to its information, some of which may be completely outside the digital realm (e.g., infiltration of organizing meetings by a political adversary). As no set of checklists can address all situations, these checklists do not represent a complete solution for securing your organization.

It is also important to recognize that implementing new security practices puts pressure on key organizational processes and personnel. Implementing the checklist recommendations will generally not immediately make your work smoother and easier. Instead, many will likely create some disruption and training needs. In order to make meaningful strides in security, your organization must be prepared to make these trade-offs.

These investments in time and attention will repay the organization in smoother, more tightly defined operations--as well as peace of mind that come from knowing that those operations protect your data and systems.

## Who are these checklists for?

Different and changing contexts, whether technical or geopolitical, introduce a variety of threats, vulnerabilities, and adversaries to consider in managing risk. To make these checklists useful, we have designed them to apply to a specific set of organizations and set of threats.

These checklists target organizations broadly seeking to protect themselves from security threats from non-persistent adversaries with limited resources (e.g., disgruntled individuals, identity thieves, political opponents, internal threats) rather than advanced persistent threats such as the U.S. government, other governments, or other large global entities including multinational corporations.

**If your threat model includes advanced persistent threats, you will need to contact a digital security professional to help you build security practices and systems beyond those recommended in these checklists in order to remain resilient in your specific context. [Contact RoadMap](mailto:info@roadmapconsulting.org) or [Information Ecology](https://iecology.org/contact) for help or referrals.**

To keep recommendations actionable, we also made some assumptions about the operations of the organizations using these checklists.

-   The organization has a staff of under 50 with an in-house technical team of no more than three, if any technical staff at all.

-   The organization uses primarily desktop and laptop computers, with some use of mobile devices to access its information systems.

-   The organization uses networks for work that are free from malicious outside interference and are segmented from the open Internet by a password-protected firewall device running up-to-date software, controlled by the owner.

-   Although the organization may communicate with partners abroad, its staff neither cross international borders while carrying the organization's equipment or data, nor regularly work in a foreign country.

-   The organization can in general successfully protect physical access to its office spaces, network equipment, and devices.

While these practices can certainly be adopted in environments that don't meet this profile, in those cases our rating system may not be accurate, and all recommendations should be reviewed by your technical or security support personnel.

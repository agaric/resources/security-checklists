---
document set: DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Introduction
author: Jonah Silas Sheridan, Lisa Jervis for Information Ecology
last modified: 11/29/2017
version: "2.01, PEER REVIEWED"
---
# Introduction

This set of documents, written with a US-based audience in mind, helps grassroots groups and not-for-profit organizations secure their information and communications from threats. At its heart is a group of checklists focused around a range of topics identified by consultants in the field that intentionally recommend constrained, accessible practices to help protect against widely available attacks on networks and devices. Adopting them will help you minimize security incidents - and the disruption and cost of viruses, malware, ransomware and phishing attempts. While these checklists can't fully protect you against powerful or persistent adversaries, the practices in them can make it harder for such enemies to attack your systems.

In addition to these lists of practices, this set contains a number of other tools and resources to help you succeed at implementing them:

* Immediately following this introduction you will find [a tool](2_readiness_assessment_tool.md) with instructions for assessing an organization's existing capacities and areas to develop in order to successfully take on this type of work. Completing this tool is recommended as a first step for all organizations.

* [Directions and a legend](3_directions_and_legend.md) explaining how to understand and use the checklists.

* [The included glossary](A_glossary.md) is meant to make digital security terms accessible to non-technical audiences and may be useful to print and share as part of training content.

* The [Assumed Threat Model](B_threat_model.md) appendix is meant for technical readers' reference. Recommendations are not annotated with specific threats mitigated at this time, but a technical support professional can help match assumed adversary capabilities with recommendations.

* Information on these checklists' origins and design are covered in the [Frequently Asked Questions](C_FAQ.md).

## About digital security

Digital security is a popular topic these days at conferences, in the media, and even around the dinner table. Yet in the deluge of information about nation-state actors, large scale attacks, and major vulnerabilities, taking action remains difficult for small organizations. Staff are often wondering, What does digital security even mean? And how can I get some?

Really, digital security just means the set of practices used to manage the risk of bad things happening due to your organization's use of information and communications systems. That includes protecting them from being accessed, changed, or blocked by anyone or anything--internal or external, intentional or accidental--that shouldn't be able to do so. While we all spend a lot of time thinking about bad actors, sometimes the greatest risks are due to threats like fire or earthquakes.

The most effective security strategies, digital or operational, are based on the specific threats, vulnerabilities, and adversaries of your organization. This does not mean that a detailed analysis is necessary to get started improving your digital security practices. Many small U.S. organizations face a shared set of baseline threats and vulnerabilities due to similarities in their operating conditions and, frequently, reliance on the same systems and technologies. These documents are meant to help these organizations address the risks associated with these common needs as a first step in improving their security stances.

## About organizational security

The adoption of new security practices always requires a **strong organizational commitment** as well as support from **organizational leadership**, because it changes the way you and your team work together. New tools and work flows are disruptive, even as they reduce your risk. It takes ongoing attention to turn policies and procedures into habits and to ensure that secure systems are regularly updated, working properly, and free of unexpected activity.  The more you can build awareness about the particular threats your organization faces, the better you can select and commit to practices that will be useful for protecting your organization in its work. The more you can create a culture of learning and mutual support in your organization, the more success you will have in the uptake of secure tools.

In these checklists we have identified solutions and practices across a range of levels of technical skill and organizational commitment that meet common threats that many, if not all, small organizations face. However, the effectiveness of these practices to protect from real threats is directly correlated with the investment you make in implementing them. Understand that there are always trade-offs associated with implementing new tools, and effort is required of staff to learn to perform tasks in new ways.

Treat digital security as the important organizational imperative it is by resourcing it appropriately and ensuring someone in your organization is responsible and has time in their workplan to manage digital security in an ongoing way. Take the time to identify your most sensitive information and communications in order to prioritize. Provide staff and volunteers the time, support, resources, and training needed to adopt any practices you undertake from these lists. In these ways you ensure that the more you put in to securing your systems, the more you will lower your risk of bad outcomes.

***Although these practices are highly recommended, they do not in and of themselves constitute a successful security practice. Information security is an ongoing process of managing risk and no list of procedures is an adequate replacement for a thorough review of what information you are protecting, why, and from whom, paired with an organizational commitment to shifting operations to mitigate risk. Information Ecology, RoadMap Consulting, and Common Counsel are not liable for negative outcomes associated with following these practices.***

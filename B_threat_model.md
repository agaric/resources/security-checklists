---
document set: DRAFT DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Appendix B: Assumed Threat Model
author: Jonah Silas Sheridan, Lisa Jervis
version: "2.0, PEER REVIEWED"　
last modified: 10/27/17
---

# Appendix B: Assumed Threat Model

## Introduction

What follows is a simplified threat model that outlines the landscape in which these checklists are expected to be effective. You may note that many of these assumptions map to the individual items in the [readiness assessment tool](2_readiness_assessment_tool), as they are foundational to the recommendations in the checklist.

These checklists do not promise to mitigate the threats listed here in their entirety. If all items in these checklists were to be implemented across an organization, any adversary as described by this threat model would face a high bar to impacting the confidentiality, integrity, or availability of that organizations' information systems. Although not annotated with this information, many single recommendations are directly oriented at defeating one or more of the listed adversary capabilities. If there is a specific capability that is of high risk for your organization, seek guidance from a technical support professional in determining which checklist items are most appropriate for mitigation of that risk.

We list the threat model in terms of assumed technical operating conditions, end-user skills, and adversary capabilities, delivered in narrative form rather than with technical detail. We believe this adversary profile fits both common criminal adversaries as well as low-skill political or otherwise aggressive opponents of non-profit organizations' work.   

## Assumed operating conditions

* Working environment is free from physical threat and devices are not consistently stolen or destroyed.

* Work is occurring primarily on adequately powered Windows, Mac, or GNU/Linux computers with some use of Android or iOS phones for communications.

* All devices have been sourced through verifiable channels and are running official versions of operating systems.

* Devices do not cross international borders, though communications and data may.

* Work occurs using a limited set of applications and tools which are selected, administered, and managed by the organization.

* Authentication mechanisms for these systems may be open to login attempts from any device.

* Staff have regular and consistent access to the Internet to perform their work.

* Networks used to connect to the Internet may also be used by other organizations and the public--including potential adversaries.

* Networks in use do not also host publicly available servers or services.

* All organizational data is regularly backed up and available for restoration in a reasonable time period in most disaster circumstances.

## Assumed end-user capabilities

* End users can physically protect their hardware and devices inside their homes and offices as well as when in public spaces.

* There is a mechanism for and end-user availability to provide/receive training in information systems topics.

* End users can operate the limited set of applications and tools their organization supplies for their use effectively.

* End users can install browser extensions on their devices. End users, technology-responsible staff or technical support providers can install other applications on end-user devices.

* End users can remember strings of letters, numbers, and symbols of length 12 or more for use as passphrases or shared secrets for accessing systems.

* Passphrases or shared secrets are used to authenticate a single or small group of individuals to a system.

* End users know how to request and receive technical support for problems with their information systems.

* End users know how to request files from backup repositories.

## Adversary assumed capabilities

* Adversary can connect to publicly available information systems and attempt to authenticate with them.

* Adversary can send arbitrary content, including spoofed headers, malware executables, infected documents, and links to email addresses.

* Adversary can send arbitrary content to smartphones via SMS or other open messaging platforms.

* Adversary can use promiscuous mode on their networking devices to collect wireless network traffic from all networks.

* Adversary can use collected WEP encrypted wireless traffic to determine the password for that network and decrypt all content.

* Adversary can collect user credentials from unsecured exchanges on wireless networks with which they can authenticate or whose passive traffic they can otherwise decrypt.

* Adversary can set up wireless access points (WAP) in any public place with arbitrary or spoofed SSIDs.

* Adversary can use routing attacks to route traffic on public shared networks through their devices.

* Adversary can take over poorly configured or secured commodity gateway routing equipment using well-known credentials or attacks on out-of-date firmware sets.

* Adversary can spoof DHCP server announcements on public shared networks to attempt to act as the gateway for that network.

* Adversary with appropriate position (via routing/DHCP attacks, WAP spoofing, or router takeovers) can perform [man-in-the-middle (MITM) attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) on unauthenticated traffic, including returning arbitrary results to DNS queries, downgrading STARTSSL email submission, rewriting unauthenticated exchanges, and sniffing credentials or other content.

* Adversary cannot generate or purchase certificates for arbitrary domains from commonly trusted Certificate Authorities (CAs) to MITM CA-mediated authenticated connections.

* Adversary can scan devices to identify their operating system or other software versions.

* Adversary can exploit well-known vulnerabilities in operating systems or local software with open listening ports.

* Adversary may be able to perform [evil maid attacks](http://searchsecurity.techtarget.com/definition/evil-maid-attack) on hardware that they have physical access to.

* Adversary may be able to use brute force mechanisms on hardware that they take possession of.

* Adversary cannot brute force encrypted information other than as noted in this document.

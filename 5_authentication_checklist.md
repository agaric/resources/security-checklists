---
document set: DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Password and Authentication Safety Checklist
author: Jonah Silas Sheridan, Lisa Jervis
version: "2.01, PEER REVIEWED"
last modified: 11/27/17
---

# Password and Authentication Safety Checklist

## Introduction

*This checklist comes from the Weathering the Storms toolkit, which contains wraparound documentation including an [introduction](1_checklist_introduction.md), [frequently asked questions](C_FAQ.md), and a [glossary](A_glossary.md) where you can look up any terms that are unfamiliar to you. This is a community-driven document set with the latest version always at [https://ecl.gy/sec-check](https://ecl.gy/sec-check). We welcome your feedback via RoadMap, or our contact form at [https://iecology.org/contact/](https://iecology.org/contact/).*

This checklist provides a number of practices that can help you and your staff better curate your organization's passwords and control who accesses your information. While passwords are the most common form of authentication (that is, proving your identity to a computer system), other systems are emerging that offer better protection. Some are mentioned below.

In the recommendations below, the term “organizational” is used to refer to the group of accounts that grant access to your organization's online identity, backups, administrative controls, and other critical systems. These tend to be used infrequently, but are very powerful. As such these passwords should be treated different from what we are calling “everyday” credentials (the set of passwords that staff members need to perform their regular duties with databases, communication tools, and other platforms used for daily work).

## Key

:heavy_check_mark: Record actions  
:rocket: Implementation management overhead   
:wrench: Technical skill level required  
:fire: Work flow disruption for staff

## Password and Authentication Security

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have all staff use password manager software.**  
:rocket: :rocket: :wrench: :wrench: :fire: :fire: :fire:    
*Since passwords can be used both to access organizational information and disrupt your work in various ways, they are one of the most important pieces of information to protect from exposure. They should never be stored in spreadsheets, text files, or word processing documents (even password-protected ones, as these are simple to break open); they should also not be saved to your browser's built-in password-saving feature.*

*Instead, use dedicated password manager software. This type of software will store all of your passwords securely and support you in adopting many of the practices listed in the items below. To use a password manager, you just remember a single password that opens up your secure file or account, which in turn stores all of your other passwords.*

*There are two types of password managers: those that are web-based and those that store information locally on your hard drive. Local storage is more secure, as web browsers are insecure environments for password storage and handling. KeePass and KeePassX are two versions of a highly recommended local password manager. These two tools use the same encrypted file format and can run on almost any computer. The excellent Security In a Box website has a [KeePass overview](https://securityinabox.org/en/guide/keepass/windows) (https://securityinabox.org/en/guide/keepass/windows).*

*We realize that web-based password managers (such as LastPass and 1Password) are efficient and appealing because they provide access to passwords where they are most often used: in a web browser. And although evaluating online services and their current security claims is outside of the scope of this document, we acknowledge that online password management tools often have adequate security levels for many organizations' everyday password handling needs; however, the benefits do not outweigh the risks when storing rarely used core organizational passwords or other highly sensitive information. (See "Separate organizational and everyday passwords" below for more on this.)*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Teach everyone in your organization to generate strong passwords and make sure they are used for all accounts, both organizational and everyday.**  
:rocket: :rocket: :wrench: :fire: :fire:   
*Strong passwords are generally 12 characters or longer and use a mix of two or three different types of characters (e.g., symbols, numbers, and both upper- and lowercase letters). Don't put uppercase letters, symbols, or digits specifically at only the beginning or end of your passwords; instead, mix them in throughout. Do not include any personal information like your favorite sports teams, places you have lived, your kids' or pets' names, important dates, or common phrases such as song lyrics or poems. Don't use patterns like "123" or "xyz," especially ones that appear on a keyboard, or acronyms associated with your work or organization.*   

*There are many ways to generate strong passwords. There is a guide in [Security In a Box](https://securityinabox.org/en/guide/passwords) (https://securityinabox.org/en/guide/passwords), and most password managers will also make a random password for you, as will other available software for that specific purpose. [Diceware](http://world.std.com/~reinhold/diceware.html) (http://world.std.com/~reinhold/diceware.html) is a fun and effective scheme for creating random yet memorable passwords using everyday objects and a word list. One other great way to make a strong password is to come up with a silly sentence that no one’s ever said before and use the first letter or two of each word as your password, mixing in other types of characters.*

*It is important to apply strong passwords to all accounts, as access to a single account can often be leveraged into access to other systems. This is especially relevant for any email accounts that can be used to reset or recover other passwords (usually via a "forgot password" link).*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Don't use the same password for more than one site or service.**  
:rocket: :wrench: :fire: :fire:   
*Following this practice is a great way to minimize the risk of using third-party technology services. If you don't reuse passwords, someone learning your username and password for one service through a leak or break-in won't make it easy to access the other accounts you use. Use different passwords for each service so you aren't relying on the services you're logging into to protect your most important secret.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Try to limit hard-copy written password storage.**  
:rocket: :wrench: :fire:  
*Even when using a password manager, there are generally a few passwords you'll need to remember without it: the password to the password manager itself, of course, and probably at least one device password. It can be tempting--and risky--to keep these written down on paper. Instead, use techniques found in the Security In a Box online guide listed above to create memorable but strong passwords. If you need a written copy of your password when you first start using it, protect it physically by storing it someplace where it won't be lost or stolen and easily identified with you. Try to type your password with less looking at the copy each time, and destroy the paper copy when you have memorized the password.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Do not tell anyone else your password(s), ever.**  
:rocket: :wrench: :fire:  
*Even if someone claims to be from IT or technical support, do not give them your password. Nearly every system allows for administrative reset of passwords for maintenance. Any legitimate IT person can use this function instead of asking you. This system also this creates an auditable trail of access to your account, and alerts you to a reset. You will need to change your password again after such admin access, but taking that extra step will ensure that you and only you have access to your digital information, and that you can know who in your organization is responsible for what changes to your account.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Consider making single-use passwords for sites you rarely use.**  
:rocket: :wrench: :fire: :fire:     
*If you never store a password, you can never leak it and it can never get stolen from you. Most service providers allow you to reset a password by sending you an email. Creating and then immediately forgetting/not recording a long, random password is a good strategy when all of the following conditions are met: 1) The account is linked to an email address that you are sure you will control in the future, 2) the account is one you will not use frequently, and 3) you can absorb a potential delay in accessing the account if/when you need to.*

*When using this method, the next time you need to log in to the account, you can hit the “forgot password" link and go through the system's password reset process. Recognize that the security of any account for which you use this single-use method becomes the same as the security for your linked email account (since you use that email account to get back into the service)--so you need to ensure you have long-term access to that email account by ensuring that the domain remains registered in the long term and that the account password is strong and stored carefully.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Wherever available, and especially for critical accounts, implement two-factor authentication using a method other than text messaging as your second factor.**  
:rocket: :rocket: :wrench: :wrench: :fire: :fire: :fire:  
*Many service providers have begun to offer login systems that rely on more than one piece of information to identify a user, also called multi-factor authentication. There can be several items used for authentication, but usually there are just two: something you know (your password) and something else you have. In this case we call it "two factor authentication" since there are two things, or "factors." Often the second factor is a code sent by text message to your phone (not recommended--see below), but it can also be embedded on a special type of USB device, a program that generates codes on your phone, or even a piece of paper with preprinted codes.*

*Two-factor authentication adds a layer of protection to your accounts so that it is much harder to take them over. It adds an extra step for people to get used to, but is a strong way to protect important accounts against weak or leaked passwords, as it protects from someone who obtains just the password from getting into the account. It is especially important to use two-factor authentication on accounts that grant a lot of access to your devices or files. Such accounts include those that allow you to push software installs to devices (such as Apple IDs or Google Play accounts) or to reset passwords for other accounts (any account you use as a recovery email for other services, for example).*

*Be aware that cell phone-based authentication factors (whether via a text message or an app) require a working phone, so if adopting them you may wish to also provide staff with backup batteries to ensure they can always log in to their accounts even on days with heavy telephone use. In some cases, where login is only occasional, a piece of paper with preprinted backup codes may suffice, but then you need to protect that paper carefully.*

*A newer and very strong second factor is a code embedded on a special type of USB device also known as a "Universal 2nd Factor" (U2F) such as a the [Yubikey](https://www.yubico.com). Not having a dependency on a working phone or cell signal is one of many advantages of U2F devices. Major services your organization may use that support U2F keys include Dashlane, Dropbox, [Facebook](https://techsolidarity.org/resources/security_key_facebook.html ), Google ([Gmail](https://techsolidarity.org/resources/security_key_gmail.htm )), Salesforce, and [Twitter](https://techsolidarity.org/resources/security_key_twitter.html ). You can read more about U2F at [https://www.yubico.com/solutions/fido-u2f/](https://www.yubico.com/solutions/fido-u2f/).*

***Text message-based codes are not recommended for use as a second factor.***  *It can be surprisingly easy for someone to take over control of a cell number via social engineering and/or fraud (see [https://www.ftc.gov/news-events/blogs/techftc/2016/06/your-mobile-phone-account-could-be-hijacked-identity-thief](https://www.ftc.gov/news-events/blogs/techftc/2016/06/your-mobile-phone-account-could-be-hijacked-identity-thief), [https://techcrunch.com/2016/06/10/how-activist-deray-mckessons-twitter-account-was-hacked/](https://techcrunch.com/2016/06/10/how-activist-deray-mckessons-twitter-account-was-hacked/), and [https://threatpost.com/nist-recommends-sms-two-factor-authentication-deprecation/119507/](https://threatpost.com/nist-recommends-sms-two-factor-authentication-deprecation/119507/) for more information). Adopt one of the other mechanisms above instead.*

*Access Now, an organization that "defends and extends the digital rights of users at risk around the world," has released a [clear and handy guide to choosing a two-factor authentication method](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png ).*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Separate organizational and everyday passwords.**  
:rocket: :rocket: :rocket: :wrench: :wrench: :fire: :fire: :fire:  
*Organizational passwords include any passwords that grant administrative control of your organization's information systems or online identity. These are very powerful credentials and so should be stored separately from passwords that just get staff into their personal user accounts. You can do this by making a separate login or file in your password manager application, or by choosing a completely different manager altogether.*

*Placing organizational passwords in a KeePass or otherwise encrypted file that only a few key staff members can access will lessen the risks of adopting an online password manager for everyday passwords, but will also place a burden on those staff members. Balancing these needs should be factored into your decision.*

:heavy_check_mark:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Set minimum password lengths and enforce complexity rules on services where you can do so, and regularly monitor user password strength.**  
:rocket: :rocket: :rocket: :rocket: :wrench: :wrench: :wrench: :wrench: :fire: :fire: :fire:   
*On many platforms, including Windows Active Directory and Google Apps, you can set controls at an administrative level to ensure that people use strong passwords. It takes some advance planning and staff training, as setting up these controls without being clear on the implications can confuse users and lock people out of their computers or work files. In addition, someone will need to be designated as the point person for resolving problems that arise from these controls. However, this step improves the security of all users at one time, so is highly recommended.*
